<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0"
      xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
      xmlns:fo="http://www.w3.org/1999/XSL/Format">
  <xsl:output method="xml" indent="yes"/>
  <xsl:template match="/countries">
  
    <fo:root xmlns:fo="http://www.w3.org/1999/XSL/Format">
    	<fo:layout-master-set>
	         <fo:simple-page-master master-name="mytemplate" page-height="29.7cm" page-width="21.0cm"
	                                margin-top="1.5cm" margin-bottom="1.5cm" margin-left="1.5cm" margin-right="1.5cm">
	          
	            <fo:region-body margin-top="1cm" margin-bottom="0.2cm" />
	            <fo:region-before extent="1cm"/>
	            <fo:region-after extent="0.5cm"/>
	            
	         </fo:simple-page-master>
    	</fo:layout-master-set>

    	<fo:page-sequence master-reference="mytemplate">
        <fo:static-content flow-name="xsl-region-after">
          <fo:block text-align="center">
            <fo:page-number/>
          </fo:block>
        </fo:static-content>
	        <fo:flow flow-name="xsl-region-body">
          <fo:block page-break-before="always" font-size="18pt" text-align="left" font-weight="bold" space-before="10px">
            Countries
          </fo:block>
          <fo:list-block provisional-distance-between-starts="10px" start-indent="10px" font-size="10pt" space-before="10px">
                  <xsl:for-each select="country" >
                    <fo:list-item>
                          <fo:list-item-label end-indent="label-end()">
                            <fo:block>-</fo:block>
                          </fo:list-item-label>
                          <fo:list-item-body start-indent="body-start()">
                            <fo:block>
                              <fo:basic-link text-decoration="underline">
                                <xsl:attribute name="internal-destination">
                                  <xsl:value-of select="@cid"/>
                                </xsl:attribute>
                                <xsl:value-of select="name" />
                              </fo:basic-link>
                            </fo:block>
                          </fo:list-item-body>
                    </fo:list-item>
                    <fo:list-item>
                          <fo:list-item-label end-indent="label-end()">
                            <fo:block> </fo:block>
                          </fo:list-item-label>
                          <fo:list-item-body start-indent="body-start()">
                            <fo:list-block>
                              <xsl:for-each select="*[position()>1]">
                                <fo:list-item>
                                  <fo:list-item-label end-indent="label-end()">
                                    <fo:block>-</fo:block>
                                  </fo:list-item-label>
                                  <fo:list-item-body start-indent="body-start()">
                                    <fo:block>
                                      <fo:basic-link text-decoration="underline">
                                        <xsl:attribute name="internal-destination">
                                          <xsl:value-of select="@id"/>
                                        </xsl:attribute>
                                        <xsl:value-of select="name(.)"/>
                                      </fo:basic-link>
                                    </fo:block>
                                  </fo:list-item-body>
                                </fo:list-item>
                              </xsl:for-each>
                            </fo:list-block>
                          </fo:list-item-body>
                    </fo:list-item>
                  </xsl:for-each>
          </fo:list-block>
          <xsl:for-each select="country">
	        	<fo:block page-break-before="always" font-size="18pt" text-align="left" font-weight="bold" space-before="10px">
                <xsl:attribute name="id">
                  <xsl:value-of select="@cid"/>
                </xsl:attribute>
	            	<xsl:value-of select="name" />
	            </fo:block>
	            <fo:block font-size="14pt" text-align="left" font-weight="bold" space-before="10px" space-after="7px">
                <xsl:attribute name="id">
                  <xsl:value-of select="background/@id"/>
                </xsl:attribute>
	            	Background
	            </fo:block>
	            <xsl:for-each select="background/par">
	            	<fo:block font-size="10pt" text-align="justify" space-before="3px">
	            		<xsl:value-of select="." />
	            	</fo:block>
	            </xsl:for-each>
	            <fo:block font-size="14pt" text-align="left" font-weight="bold" space-before="10px">
                <xsl:attribute name="id">
                  <xsl:value-of select="geography/@id"/>
                </xsl:attribute>
	            	Geography
	            </fo:block>
	            <fo:block font-size="12pt" text-align="left" font-weight="bold" space-before="10px">
	            	Location
	            </fo:block>
	            <fo:block font-size="10pt" text-align="left" space-before="10px">
	            	<xsl:value-of select="geography/location" />
	            </fo:block>
	            <fo:block font-size="12pt" text-align="left" font-weight="bold" space-before="10px">
	            	Coordinates
	            </fo:block>
	            <fo:list-block provisional-distance-between-starts="10px"
                  start-indent="10px" font-size="10pt" space-before="10px">
                	<fo:list-item>
                        <fo:list-item-label end-indent="label-end()">
                          <fo:block></fo:block>
                        </fo:list-item-label>
                        <fo:list-item-body start-indent="body-start()">
                          <fo:block>Latitude: <xsl:value-of select="geography/coordinates/latitude" /></fo:block>
                        </fo:list-item-body>
                	</fo:list-item>
                	<fo:list-item>
                        <fo:list-item-label end-indent="label-end()">
                          <fo:block></fo:block>
                        </fo:list-item-label>
                        <fo:list-item-body start-indent="body-start()">
                          <fo:block>Longitude: <xsl:value-of select="geography/coordinates/longitude" /></fo:block>
                        </fo:list-item-body>
                	</fo:list-item>
                </fo:list-block>
	            <fo:block font-size="12pt" text-align="left" font-weight="bold" space-before="10px">
	            	Area
	            </fo:block>
	            <fo:list-block provisional-distance-between-starts="10px"
                  start-indent="10px" font-size="10pt" space-before="10px">
                	<fo:list-item>
                        <fo:list-item-label end-indent="label-end()">
                          <fo:block></fo:block>
                        </fo:list-item-label>
                        <fo:list-item-body start-indent="body-start()">
                          <fo:block>Total: <xsl:value-of select="geography/area/a_total" /></fo:block>
                        </fo:list-item-body>
                	</fo:list-item>
                	<fo:list-item>
                        <fo:list-item-label end-indent="label-end()">
                          <fo:block></fo:block>
                        </fo:list-item-label>
                        <fo:list-item-body start-indent="body-start()">
                          <fo:block>Land: <xsl:value-of select="geography/area/a_land" /></fo:block>
                        </fo:list-item-body>
                	</fo:list-item>
                	<fo:list-item>
                        <fo:list-item-label end-indent="label-end()">
                          <fo:block></fo:block>
                        </fo:list-item-label>
                        <fo:list-item-body start-indent="body-start()">
                          <fo:block>Water: <xsl:value-of select="geography/area/a_water" /></fo:block>
                        </fo:list-item-body>
                	</fo:list-item>
                </fo:list-block>
	            <fo:block font-size="12pt" text-align="left" font-weight="bold" space-before="10px">
	            	Climate
	            </fo:block>
	            <fo:block font-size="10pt" text-align="left" space-before="10px">
	            	<xsl:value-of select="geography/climate" />
	            </fo:block>
	            <fo:block font-size="12pt" text-align="left" font-weight="bold" space-before="10px">
	            	Terrain
	            </fo:block>
	            <fo:block font-size="10pt" text-align="left" space-before="10px">
	            	<xsl:value-of select="geography/terrain" />
	            </fo:block>
	            <fo:block font-size="12pt" text-align="left" font-weight="bold" space-before="10px">
	            	Resources
	            </fo:block>
	            <fo:block font-size="10pt" text-align="left" space-before="10px">
	            	<xsl:for-each select="geography/resources/resource">
						<xsl:value-of select="." />
						<xsl:if test="not (position()=last())">
							<xsl:text>, </xsl:text>
						</xsl:if>
					</xsl:for-each>
	            </fo:block>
	            <fo:block font-size="12pt" text-align="left" font-weight="bold" space-before="10px">
	            	Land
	            </fo:block>
	            <fo:list-block provisional-distance-between-starts="10px"
                  start-indent="10px" font-size="10pt" space-before="10px">
                	<fo:list-item>
                        <fo:list-item-label end-indent="label-end()">
                          <fo:block></fo:block>
                        </fo:list-item-label>
                        <fo:list-item-body start-indent="body-start()">
                          <fo:block>Agricultural: <xsl:value-of select="geography/land/l_agricultural" /></fo:block>
                        </fo:list-item-body>
                	</fo:list-item>
                	<fo:list-item>
                        <fo:list-item-label end-indent="label-end()">
                          <fo:block></fo:block>
                        </fo:list-item-label>
                        <fo:list-item-body start-indent="body-start()">
                          <fo:block>Forest: <xsl:value-of select="geography/land/l_forest" /></fo:block>
                        </fo:list-item-body>
                	</fo:list-item>
                	<fo:list-item>
                        <fo:list-item-label end-indent="label-end()">
                          <fo:block></fo:block>
                        </fo:list-item-label>
                        <fo:list-item-body start-indent="body-start()">
                          <fo:block>Other: <xsl:value-of select="geography/land/l_outher" /></fo:block>
                        </fo:list-item-body>
                	</fo:list-item>
                </fo:list-block>
	            <fo:block font-size="14pt" text-align="left" font-weight="bold" space-before="10px">
                <xsl:attribute name="id">
                  <xsl:value-of select="society/@id"/>
                </xsl:attribute>
	            	Society
	            </fo:block>
	            <fo:block font-size="12pt" text-align="left" font-weight="bold" space-before="10px">
	            	Population
	            </fo:block>
	            <fo:block font-size="10pt" text-align="left" space-before="10px">
	            	<xsl:value-of select="society/population" />
	            </fo:block>
	            <fo:block font-size="12pt" text-align="left" font-weight="bold" space-before="10px">
	            	Nationality
	            </fo:block>
	            <fo:list-block provisional-distance-between-starts="10px"
                  start-indent="10px" font-size="10pt" space-before="10px">
                	<fo:list-item>
                        <fo:list-item-label end-indent="label-end()">
                          <fo:block></fo:block>
                        </fo:list-item-label>
                        <fo:list-item-body start-indent="body-start()">
                          <fo:block>Noun: <xsl:value-of select="society/nationality/n_noun" /></fo:block>
                        </fo:list-item-body>
                	</fo:list-item>
                	<fo:list-item>
                        <fo:list-item-label end-indent="label-end()">
                          <fo:block></fo:block>
                        </fo:list-item-label>
                        <fo:list-item-body start-indent="body-start()">
                          <fo:block>Longitude: <xsl:value-of select="society/nationality/n_adjective" /></fo:block>
                        </fo:list-item-body>
                	</fo:list-item>
                </fo:list-block>
	            <fo:block font-size="12pt" text-align="left" font-weight="bold" space-before="10px">
	            	Etnicity
	            </fo:block>
              <fo:list-block provisional-distance-between-starts="10px"
                  start-indent="10px" font-size="10pt" space-before="10px">
                  <xsl:for-each select="society/etnicity/etnic_group" >
                    <fo:list-item>
                          <fo:list-item-label end-indent="label-end()">
                            <fo:block></fo:block>
                          </fo:list-item-label>
                          <fo:list-item-body start-indent="body-start()">
                            <fo:block>
                              <xsl:value-of select="eg_name" />: <xsl:value-of select="eg_percentage" />
                            </fo:block>
                          </fo:list-item-body>
                    </fo:list-item>
                  </xsl:for-each>
                </fo:list-block>
				<xsl:if test="count(/society/etnicity/e_note)=1">
					<fo:block font-size="10pt" text-align="left" space-before="3px">
						note: <xsl:value-of select="society/etnicity/e_note" />
					</fo:block>
				</xsl:if>
	            <fo:block font-size="12pt" text-align="left" font-weight="bold" space-before="10px">
	            	Language
	            </fo:block>
	            <fo:block font-size="10pt" text-align="left" space-before="10px">
	            	<xsl:value-of select="society/languages/official" />
	            	<xsl:text>(official)</xsl:text>
	            	<xsl:for-each select="society/languages/language">
						<xsl:text>, </xsl:text>
						<xsl:value-of select="." />
					</xsl:for-each>
	            </fo:block>
	            <fo:block font-size="12pt" text-align="left" font-weight="bold" space-before="10px">
	            	Religion
	            </fo:block>
              <fo:list-block provisional-distance-between-starts="10px"
                  start-indent="10px" font-size="10pt" space-before="10px">
                  <xsl:for-each select="society/religions/religion" >
                    <fo:list-item>
                          <fo:list-item-label end-indent="label-end()">
                            <fo:block></fo:block>
                          </fo:list-item-label>
                          <fo:list-item-body start-indent="body-start()">
                            <fo:block>
                              <xsl:value-of select="r_name" />: <xsl:value-of select="r_percentage" />
                            </fo:block>
                          </fo:list-item-body>
                    </fo:list-item>
                  </xsl:for-each>
                </fo:list-block>
				<xsl:if test="count(/society/religions/r_note)=1">
					<fo:block font-size="10pt" text-align="left" space-before="3px">
						note: <xsl:value-of select="society/religions/r_note" />
					</fo:block>
				</xsl:if>
	            <fo:block font-size="12pt" text-align="left" font-weight="bold" space-before="10px">
	            	Age
	            </fo:block>
              <fo:list-block provisional-distance-between-starts="10px"
                  start-indent="10px" font-size="10pt" space-before="10px">
                  <xsl:for-each select="society/age/a_group" >
                    <fo:list-item>
                          <fo:list-item-label end-indent="label-end()">
                            <fo:block></fo:block>
                          </fo:list-item-label>
                          <fo:list-item-body start-indent="body-start()">
                            <fo:block>
                <xsl:text>Age group of </xsl:text>
                <xsl:value-of select="ag_range" />
                <xsl:text>: </xsl:text>
                <xsl:value-of select="ag_percentage" />
                <xsl:text> of total population (</xsl:text>
                <xsl:value-of select="ag_male" />
                <xsl:text>  male/</xsl:text>
                <xsl:value-of select="ag_female" />
                <xsl:text>  female)</xsl:text>
                            </fo:block>
                          </fo:list-item-body>
                    </fo:list-item>
                  </xsl:for-each>
                </fo:list-block>
                <fo:block font-size="10pt" text-align="left" space-before="3px">
                	Median:
                </fo:block>
	            <fo:list-block provisional-distance-between-starts="10px"
                  start-indent="10px" font-size="10pt" space-before="3px">
                	<fo:list-item>
                        <fo:list-item-label end-indent="label-end()">
                          <fo:block></fo:block>
                        </fo:list-item-label>
                        <fo:list-item-body start-indent="body-start()">
                          <fo:block>Total: <xsl:value-of select="society/age/a_median/am_total" /></fo:block>
                        </fo:list-item-body>
                	</fo:list-item>
                	<fo:list-item>
                        <fo:list-item-label end-indent="label-end()">
                          <fo:block></fo:block>
                        </fo:list-item-label>
                        <fo:list-item-body start-indent="body-start()">
                          <fo:block>Male: <xsl:value-of select="society/age/a_median/am_male" /></fo:block>
                        </fo:list-item-body>
                	</fo:list-item>
                	<fo:list-item>
                        <fo:list-item-label end-indent="label-end()">
                          <fo:block></fo:block>
                        </fo:list-item-label>
                        <fo:list-item-body start-indent="body-start()">
                          <fo:block>Female: <xsl:value-of select="society/age/a_median/am_female" /></fo:block>
                        </fo:list-item-body>
                	</fo:list-item>
                </fo:list-block>
                <fo:block font-size="10pt" text-align="left" space-before="3px">
                	Life expectency:
                </fo:block>
	            <fo:list-block provisional-distance-between-starts="10px"
                  start-indent="10px" font-size="10pt" space-before="3px">
                	<fo:list-item>
                        <fo:list-item-label end-indent="label-end()">
                          <fo:block></fo:block>
                        </fo:list-item-label>
                        <fo:list-item-body start-indent="body-start()">
                          <fo:block>Total: <xsl:value-of select="society/age/a_life_expectency/al_total" /></fo:block>
                        </fo:list-item-body>
                	</fo:list-item>
                	<fo:list-item>
                        <fo:list-item-label end-indent="label-end()">
                          <fo:block></fo:block>
                        </fo:list-item-label>
                        <fo:list-item-body start-indent="body-start()">
                          <fo:block>Male: <xsl:value-of select="society/age/a_life_expectency/al_male" /></fo:block>
                        </fo:list-item-body>
                	</fo:list-item>
                	<fo:list-item>
                        <fo:list-item-label end-indent="label-end()">
                          <fo:block></fo:block>
                        </fo:list-item-label>
                        <fo:list-item-body start-indent="body-start()">
                          <fo:block>Female: <xsl:value-of select="society/age/a_life_expectency/al_female" /></fo:block>
                        </fo:list-item-body>
                	</fo:list-item>
                </fo:list-block>
	            <fo:block font-size="12pt" text-align="left" font-weight="bold" space-before="10px">
	            	Urbanization
	            </fo:block>
                <fo:block font-size="10pt" text-align="left" space-before="10px">
                	People living in urban areas: 
					<xsl:value-of select="society/urbanization/u_population" />
					 of total population
                </fo:block>
                <fo:block font-size="10pt" text-align="left" space-before="3px">
                	Rate of urbanization: 
					<xsl:value-of select="society/urbanization/u_rate" />
                </fo:block>
              <fo:list-block provisional-distance-between-starts="10px"
                  start-indent="10px" font-size="10pt" space-before="3px">
                  <xsl:for-each select="society/urbanization/u_major_areas/u_major_area" >
                    <fo:list-item>
                          <fo:list-item-label end-indent="label-end()">
                            <fo:block></fo:block>
                          </fo:list-item-label>
                          <fo:list-item-body start-indent="body-start()">
                            <fo:block>
                              <xsl:value-of select="uma_name" />: <xsl:value-of select="uma_population" />
                            </fo:block>
                          </fo:list-item-body>
                    </fo:list-item>
                  </xsl:for-each>
                </fo:list-block>
                <xsl:if test="count(/society/literacy)=1">
		            <fo:block font-size="12pt" text-align="left" font-weight="bold" space-before="10px">
		            	Literacy
		            </fo:block>
		            <fo:list-block provisional-distance-between-starts="10px"
	                  start-indent="10px" font-size="10pt" space-before="10px">
	                	<fo:list-item>
	                        <fo:list-item-label end-indent="label-end()">
	                          <fo:block></fo:block>
	                        </fo:list-item-label>
	                        <fo:list-item-body start-indent="body-start()">
	                          <fo:block>Total: <xsl:value-of select="society/literacy/l_total" /></fo:block>
	                        </fo:list-item-body>
	                	</fo:list-item>
	                	<fo:list-item>
	                        <fo:list-item-label end-indent="label-end()">
	                          <fo:block></fo:block>
	                        </fo:list-item-label>
	                        <fo:list-item-body start-indent="body-start()">
	                          <fo:block>Male: <xsl:value-of select="society/literacy/l_male" /></fo:block>
	                        </fo:list-item-body>
	                	</fo:list-item>
	                	<fo:list-item>
	                        <fo:list-item-label end-indent="label-end()">
	                          <fo:block></fo:block>
	                        </fo:list-item-label>
	                        <fo:list-item-body start-indent="body-start()">
	                          <fo:block>Female: <xsl:value-of select="society/literacy/l_female" /></fo:block>
	                        </fo:list-item-body>
	                	</fo:list-item>
	                </fo:list-block>
                </xsl:if>
	            <fo:block font-size="14pt" text-align="left" font-weight="bold" space-before="10px">
                <xsl:attribute name="id">
                  <xsl:value-of select="government/@id"/>
                </xsl:attribute>
	            	Government
	            </fo:block>
	            <fo:block font-size="12pt" text-align="left" font-weight="bold" space-before="10px">
	            	Government type
	            </fo:block>
	            <fo:block font-size="10pt" text-align="left" space-before="10px">
	            	<xsl:value-of select="government/government_type" />
	            </fo:block>
	            <fo:block font-size="12pt" text-align="left" font-weight="bold" space-before="10px">
	            	Capital
	            </fo:block>
	            <fo:block font-size="10pt" text-align="left" space-before="10px">
	            	<xsl:value-of select="government/capital" />
	            </fo:block>
	            <fo:block font-size="12pt" text-align="left" font-weight="bold" space-before="10px">
	            	Chief of state
	            </fo:block>
	            <fo:block font-size="10pt" text-align="left" space-before="10px">
	            	<xsl:value-of select="government/chief_of_state/c_name" />
	            	(elected <xsl:value-of select="government/chief_of_state/c_elected" />)
	            </fo:block>
	            <fo:block font-size="12pt" text-align="left" font-weight="bold" space-before="10px">
	            	Head of government
	            </fo:block>
	            <fo:block font-size="10pt" text-align="left" space-before="10px">
	            	<xsl:value-of select="government/head_of_goverment/h_name" />
	            	(elected <xsl:value-of select="government/head_of_goverment/h_elected" />)
	            </fo:block>
	            <fo:block font-size="12pt" text-align="left" font-weight="bold" space-before="10px">
	            	Legal system
	            </fo:block>
	            <fo:block font-size="10pt" text-align="left" space-before="10px">
	            	<xsl:value-of select="government/legal_system" />
	            </fo:block>
	            <fo:block font-size="12pt" text-align="left" font-weight="bold" space-before="10px">
	            	Symbols
	            </fo:block>
	            <fo:block font-size="10pt" text-align="left" space-before="10px">
	            	Symbols:
	            </fo:block>
	            <fo:block font-size="10pt" text-align="left" space-before="3px">
					<xsl:for-each select="government/symbols/symbol">
						<xsl:value-of select="." />
						<xsl:if test="not (position()=last())">
							<xsl:text>, </xsl:text>
						</xsl:if>
					</xsl:for-each>
	            </fo:block>
	            <fo:block font-size="10pt" text-align="left" space-before="5px">
	            	Colors:
	            </fo:block>
	            <fo:block font-size="10pt" text-align="left" space-before="3px">
					<xsl:for-each select="government/symbols/s_color">
						<xsl:value-of select="." />
						<xsl:if test="not (position()=last())">
							<xsl:text>, </xsl:text>
						</xsl:if>
					</xsl:for-each>
	            </fo:block>
	            <fo:block font-size="10pt" text-align="left" space-before="5px">
	            	Anthem:
	            </fo:block>
	            <fo:block font-size="10pt" text-align="left" space-before="3px">
	            	<xsl:value-of select="government/symbols/anthem" />
	            </fo:block>
	            <fo:block font-size="14pt" text-align="left" font-weight="bold" space-before="10px">
                <xsl:attribute name="id">
                  <xsl:value-of select="economy/@id"/>
                </xsl:attribute>
	            	Economy
	            </fo:block>
	            <fo:block font-size="12pt" text-align="left" font-weight="bold" space-before="10px" space-after="7px">
	            	Overview
	            </fo:block>
	            <xsl:for-each select="economy/e_overview/par">
	            	<fo:block font-size="10pt" text-align="justify" space-before="3px">
	            		<xsl:value-of select="." />
	            	</fo:block>
	            </xsl:for-each>
	            <fo:block font-size="12pt" text-align="left" font-weight="bold" space-before="10px">
	            	GDP
	            </fo:block>
                <fo:block font-size="10pt" text-align="left" space-before="10px">
                	GDP amount: 
					<xsl:value-of select="economy/GDP/GDP_amount" />
                </fo:block>
                <fo:block font-size="10pt" text-align="left" space-before="3px">
                	GDP growth:
					<xsl:value-of select="economy/GDP/GDP_growth" />
                </fo:block>
                <fo:block font-size="10pt" text-align="left" space-before="3px">
                	GDP per capita:
					<xsl:value-of select="economy/GDP/GDP_per_capita" />
                </fo:block>
                <fo:block font-size="10pt" text-align="left" space-before="3px">
                	GDP savings:
					<xsl:value-of select="economy/GDP/GDP_savings" />
                </fo:block>
                <fo:block font-size="10pt" text-align="left" space-before="3px">
                	GDP from agriculture:
					<xsl:value-of select="economy/GDP/GDP_sectors/GDPs_agriculture" />
                </fo:block>
                <fo:block font-size="10pt" text-align="left" space-before="3px">
                	GDP from industry:
					<xsl:value-of select="economy/GDP/GDP_sectors/GDPs_industry" />
                </fo:block>
                <fo:block font-size="10pt" text-align="left" space-before="3px">
                	GDP from services:
					<xsl:value-of select="economy/GDP/GDP_sectors/GDPs_services" />
                </fo:block>
	            <fo:block font-size="12pt" text-align="left" font-weight="bold" space-before="10px">
	            	Agricultural products
	            </fo:block>
	            <fo:block font-size="10pt" text-align="left" space-before="10px">
	            	<xsl:for-each select="economy/agricultural_products/agricultural_product">
						<xsl:value-of select="." />
						<xsl:if test="not (position()=last())">
							<xsl:text>, </xsl:text>
						</xsl:if>
					</xsl:for-each>
	            </fo:block>
	            <fo:block font-size="12pt" text-align="left" font-weight="bold" space-before="10px">
	            	Industries
	            </fo:block>
	            <fo:block font-size="10pt" text-align="left" space-before="10px">
	            	<xsl:for-each select="economy/industries/industry">
						<xsl:value-of select="." />
						<xsl:if test="not (position()=last())">
							<xsl:text>, </xsl:text>
						</xsl:if>
					</xsl:for-each>
	            </fo:block>
	            <fo:block font-size="12pt" text-align="left" font-weight="bold" space-before="10px">
	            	Labor force
	            </fo:block>
                <fo:block font-size="10pt" text-align="left" space-before="10px">
                	Total amount: 
					<xsl:value-of select="economy/labor_force/lf_amount" />
                </fo:block>
                <fo:block font-size="10pt" text-align="left" space-before="3px">
                	In agriculture: 
					<xsl:value-of select="economy/labor_force/lf_sectors/lfs_agriculture" />
                </fo:block>
                <fo:block font-size="10pt" text-align="left" space-before="3px">
                	In industry: 
					<xsl:value-of select="economy/labor_force/lf_sectors/lfs_industry" />
                </fo:block>
                <fo:block font-size="10pt" text-align="left" space-before="3px">
                	In services: 
					<xsl:value-of select="economy/labor_force/lf_sectors/lfs_services" />
                </fo:block>
	            <fo:block font-size="12pt" text-align="left" font-weight="bold" space-before="10px">
	            	Unemployment
	            </fo:block>
	            <fo:block font-size="10pt" text-align="left" space-before="10px">
	            	<xsl:value-of select="economy/unemployment" />
	            </fo:block>
	            <fo:block font-size="12pt" text-align="left" font-weight="bold" space-before="10px">
	            	Inflation
	            </fo:block>
	            <fo:block font-size="10pt" text-align="left" space-before="10px">
	            	<xsl:value-of select="economy/inflation" />
	            </fo:block>
	            <fo:block font-size="12pt" text-align="left" font-weight="bold" space-before="10px">
	            	Exports
	            </fo:block>
                <fo:block font-size="10pt" text-align="left" space-before="10px">
                	Total amount: 
					<xsl:value-of select="economy/exports_and_imports/exports/e_amount" />
                </fo:block>
                <fo:block font-size="10pt" text-align="left" space-before="3px">
                	Partners: 
					<xsl:for-each select="economy/exports_and_imports/exports/e_partners/e_partner">
						<xsl:value-of select="ep_name" />
						<xsl:text>(</xsl:text>
						<xsl:value-of select="ep_percentage" />
						<xsl:text>)</xsl:text>
						<xsl:if test="not (position()=last())">
							<xsl:text>, </xsl:text>
						</xsl:if>
					</xsl:for-each>
                </fo:block>
                <fo:block font-size="10pt" text-align="left" space-before="3px">
                	Commodities: 
					<xsl:for-each select="economy/exports_and_imports/exports/e_commodities/e_commodity">
						<xsl:value-of select="." />
						<xsl:if test="not (position()=last())">
							<xsl:text>, </xsl:text>
						</xsl:if>
					</xsl:for-each>
                </fo:block>
	            <fo:block font-size="12pt" text-align="left" font-weight="bold" space-before="10px">
	            	Imports
	            </fo:block>
                <fo:block font-size="10pt" text-align="left" space-before="10px">
                	Total amount: 
					<xsl:value-of select="economy/exports_and_imports/imports/i_amount" />
                </fo:block>
                <fo:block font-size="10pt" text-align="left" space-before="3px">
                	Partners: 
					<xsl:for-each select="economy/exports_and_imports/imports/i_partners/i_partner">
						<xsl:value-of select="ip_name" />
						<xsl:text>(</xsl:text>
						<xsl:value-of select="ip_percentage" />
						<xsl:text>)</xsl:text>
						<xsl:if test="not (position()=last())">
							<xsl:text>, </xsl:text>
						</xsl:if>
					</xsl:for-each>
                </fo:block>
                <fo:block font-size="10pt" text-align="left" space-before="3px">
                	Commodities: 
					<xsl:for-each select="economy/exports_and_imports/imports/i_commodities/i_commodity">
						<xsl:value-of select="." />
						<xsl:if test="not (position()=last())">
							<xsl:text>, </xsl:text>
						</xsl:if>
					</xsl:for-each>
                </fo:block>
	            <fo:block font-size="14pt" text-align="left" font-weight="bold" space-before="10px">
                <xsl:attribute name="id">
                  <xsl:value-of select="energy/@id"/>
                </xsl:attribute>
	            	Energy
	            </fo:block>
	            <fo:block font-size="12pt" text-align="left" font-weight="bold" space-before="10px">
	            	Electricity
	            </fo:block>
                <fo:block font-size="10pt" text-align="left" space-before="10px">
                	Access: 
					<xsl:value-of select="energy/electricity/e_access" />
                </fo:block>
                <fo:block font-size="10pt" text-align="left" space-before="3px">
                	Production:
					<xsl:value-of select="energy/electricity/e_production" />
                </fo:block>
                <fo:block font-size="10pt" text-align="left" space-before="3px">
                	Consumption:
					<xsl:value-of select="energy/electricity/e_consumption" />
                </fo:block>
                <fo:block font-size="10pt" text-align="left" space-before="3px">
                	Exports:
					<xsl:value-of select="energy/electricity/e_exports" />
                </fo:block>
                <fo:block font-size="10pt" text-align="left" space-before="3px">
                	Imports:
					<xsl:value-of select="energy/electricity/e_imports" />
                </fo:block>
                <fo:block font-size="10pt" text-align="left" space-before="7px">
                	Sources:
                </fo:block>
                <xsl:for-each select="energy/electricity/e_sources/e_source">
					<fo:block font-size="10pt" text-align="left" space-before="3px">
						<xsl:value-of select="es_type" /><xsl:text>: </xsl:text>
						<xsl:value-of select="es_percentage" />
					</fo:block>
				</xsl:for-each>
	            <fo:block font-size="12pt" text-align="left" font-weight="bold" space-before="10px">
	            	Crude oil
	            </fo:block>
	            <fo:list-block provisional-distance-between-starts="10px"
                  start-indent="10px" font-size="10pt" space-before="3px">
                	<fo:list-item>
                        <fo:list-item-label end-indent="label-end()">
                          <fo:block></fo:block>
                        </fo:list-item-label>
                        <fo:list-item-body start-indent="body-start()">
                          <fo:block>Production: <xsl:value-of select="energy/crude_oil/co_production" /></fo:block>
                        </fo:list-item-body>
                	</fo:list-item>
                	<fo:list-item>
                        <fo:list-item-label end-indent="label-end()">
                          <fo:block></fo:block>
                        </fo:list-item-label>
                        <fo:list-item-body start-indent="body-start()">
                          <fo:block>Exports: <xsl:value-of select="energy/crude_oil/co_exports" /></fo:block>
                        </fo:list-item-body>
                	</fo:list-item>
                	<fo:list-item>
                        <fo:list-item-label end-indent="label-end()">
                          <fo:block></fo:block>
                        </fo:list-item-label>
                        <fo:list-item-body start-indent="body-start()">
                          <fo:block>Imports: <xsl:value-of select="energy/crude_oil/co_imports" /></fo:block>
                        </fo:list-item-body>
                	</fo:list-item>
                </fo:list-block>
	            <fo:block font-size="12pt" text-align="left" font-weight="bold" space-before="10px">
	            	Refined petroleum products
	            </fo:block>
	            <fo:list-block provisional-distance-between-starts="10px"
                  start-indent="10px" font-size="10pt" space-before="3px">
                	<fo:list-item>
                        <fo:list-item-label end-indent="label-end()">
                          <fo:block></fo:block>
                        </fo:list-item-label>
                        <fo:list-item-body start-indent="body-start()">
                          <fo:block>Production: <xsl:value-of select="energy/refined_petroleum_products/rpp_production" /></fo:block>
                        </fo:list-item-body>
                	</fo:list-item>
                	<fo:list-item>
                        <fo:list-item-label end-indent="label-end()">
                          <fo:block></fo:block>
                        </fo:list-item-label>
                        <fo:list-item-body start-indent="body-start()">
                          <fo:block>Consumption: <xsl:value-of select="energy/refined_petroleum_products/rpp_consumption" /></fo:block>
                        </fo:list-item-body>
                	</fo:list-item>
                	<fo:list-item>
                        <fo:list-item-label end-indent="label-end()">
                          <fo:block></fo:block>
                        </fo:list-item-label>
                        <fo:list-item-body start-indent="body-start()">
                          <fo:block>Exports: <xsl:value-of select="energy/refined_petroleum_products/rpp_exports" /></fo:block>
                        </fo:list-item-body>
                	</fo:list-item>
                	<fo:list-item>
                        <fo:list-item-label end-indent="label-end()">
                          <fo:block></fo:block>
                        </fo:list-item-label>
                        <fo:list-item-body start-indent="body-start()">
                          <fo:block>Imports: <xsl:value-of select="energy/refined_petroleum_products/rpp_imports" /></fo:block>
                        </fo:list-item-body>
                	</fo:list-item>
                </fo:list-block>
	            <fo:block font-size="12pt" text-align="left" font-weight="bold" space-before="10px">
	            	Natural gas
	            </fo:block>
	            <fo:list-block provisional-distance-between-starts="10px"
                  start-indent="10px" font-size="10pt" space-before="3px">
                	<fo:list-item>
                        <fo:list-item-label end-indent="label-end()">
                          <fo:block></fo:block>
                        </fo:list-item-label>
                        <fo:list-item-body start-indent="body-start()">
                          <fo:block>Production: <xsl:value-of select="energy/natural_gas/ng_production" /></fo:block>
                        </fo:list-item-body>
                	</fo:list-item>
                	<fo:list-item>
                        <fo:list-item-label end-indent="label-end()">
                          <fo:block></fo:block>
                        </fo:list-item-label>
                        <fo:list-item-body start-indent="body-start()">
                          <fo:block>Consumption: <xsl:value-of select="energy/natural_gas/ng_consumption" /></fo:block>
                        </fo:list-item-body>
                	</fo:list-item>
                	<fo:list-item>
                        <fo:list-item-label end-indent="label-end()">
                          <fo:block></fo:block>
                        </fo:list-item-label>
                        <fo:list-item-body start-indent="body-start()">
                          <fo:block>Exports: <xsl:value-of select="energy/natural_gas/ng_exports" /></fo:block>
                        </fo:list-item-body>
                	</fo:list-item>
                	<fo:list-item>
                        <fo:list-item-label end-indent="label-end()">
                          <fo:block></fo:block>
                        </fo:list-item-label>
                        <fo:list-item-body start-indent="body-start()">
                          <fo:block>Imports: <xsl:value-of select="energy/natural_gas/ng_imports" /></fo:block>
                        </fo:list-item-body>
                	</fo:list-item>
                </fo:list-block>
                <fo:block font-size="14pt" text-align="left" font-weight="bold" space-before="10px">
                <xsl:attribute name="id">
                  <xsl:value-of select="communication/@id"/>
                </xsl:attribute>
	            	Communication
	            </fo:block>
	            <fo:block font-size="12pt" text-align="left" font-weight="bold" space-before="10px">
	            	Telephone
	            </fo:block>
                <fo:block font-size="10pt" text-align="left" space-before="10px">
                	Fixed lines subscriptions: 
					<xsl:value-of select="communication/telephones/t_fixed_lines_subscribtions" />
                </fo:block>
                <fo:block font-size="10pt" text-align="left" space-before="3px">
                	Mobile cellular subscriptions: 
					<xsl:value-of select="communication/telephones/t_mobile_cellular_subscribtions" />
                </fo:block>
	            <fo:block font-size="12pt" text-align="left" font-weight="bold" space-before="10px">
	            	Broadcast media
	            </fo:block>
                <fo:block font-size="10pt" text-align="left" space-before="10px"> 
					<xsl:value-of select="communication/broadcast_media" />
                </fo:block>
	            <fo:block font-size="12pt" text-align="left" font-weight="bold" space-before="10px">
	            	Internet
	            </fo:block>
	            <fo:list-block provisional-distance-between-starts="10px"
                  start-indent="10px" font-size="10pt" space-before="3px">
                	<fo:list-item>
                        <fo:list-item-label end-indent="label-end()">
                          <fo:block></fo:block>
                        </fo:list-item-label>
                        <fo:list-item-body start-indent="body-start()">
                          <fo:block>Internet code: <xsl:value-of select="communication/internet/i_code" /></fo:block>
                        </fo:list-item-body>
                	</fo:list-item>
                	<fo:list-item>
                        <fo:list-item-label end-indent="label-end()">
                          <fo:block></fo:block>
                        </fo:list-item-label>
                        <fo:list-item-body start-indent="body-start()">
                          <fo:block>Total users: <xsl:value-of select="communication/internet/i_total_users" /></fo:block>
                        </fo:list-item-body>
                	</fo:list-item>
                </fo:list-block>
	            <fo:block font-size="14pt" text-align="left" font-weight="bold" space-before="10px">
                <xsl:attribute name="id">
                  <xsl:value-of select="military_and_security/@id"/>
                </xsl:attribute>
	            	Military and security
	            </fo:block>
	            <fo:block font-size="12pt" text-align="left" font-weight="bold" space-before="10px">
	            	Expenditure
	            </fo:block>
	            <fo:block font-size="10pt" text-align="left" space-before="10px">
	            	<xsl:value-of select="military_and_security/expenditure" />
	            </fo:block>
	            <fo:block font-size="12pt" text-align="left" font-weight="bold" space-before="10px">
	            	Military forces
	            </fo:block>
	            <fo:block font-size="10pt" text-align="left" space-before="10px">
					<xsl:for-each select="military_and_security/military_forces/military_force" >
						<xsl:value-of select="mf_name_EN" />
						<xsl:if test="count(mf_abbreviation)=1">
							<xsl:text>(</xsl:text>
							<xsl:value-of select="mf_abbreviation" />
							<xsl:text>)</xsl:text>
						</xsl:if>
						<xsl:if test="not (position()=last())">
							<xsl:text>, </xsl:text>
						</xsl:if>
					</xsl:for-each>
	            </fo:block>
	            <fo:block font-size="12pt" text-align="left" font-weight="bold" space-before="10px">
	            	Obligation
	            </fo:block>
	            <fo:block font-size="10pt" text-align="left" space-before="10px">
	            	<xsl:value-of select="military_and_security/obligation" />
	            </fo:block>
	            <fo:block font-size="14pt" text-align="left" font-weight="bold" space-before="10px">
                <xsl:attribute name="id">
                  <xsl:value-of select="transportation/@id"/>
                </xsl:attribute>
	            	Transportation
	            </fo:block>
	            <fo:block font-size="12pt" text-align="left" font-weight="bold" space-before="10px">
	            	Airports
	            </fo:block>
	            <fo:list-block provisional-distance-between-starts="10px"
                  start-indent="10px" font-size="10pt" space-before="3px">
                	<fo:list-item>
                        <fo:list-item-label end-indent="label-end()">
                          <fo:block></fo:block>
                        </fo:list-item-label>
                        <fo:list-item-body start-indent="body-start()">
                          <fo:block>Total: <xsl:value-of select="transportation/airports/ap_total" /></fo:block>
                        </fo:list-item-body>
                	</fo:list-item>
                	<fo:list-item>
                        <fo:list-item-label end-indent="label-end()">
                          <fo:block></fo:block>
                        </fo:list-item-label>
                        <fo:list-item-body start-indent="body-start()">
                          <fo:block>Paved: <xsl:value-of select="transportation/airports/ap_paved" /></fo:block>
                        </fo:list-item-body>
                	</fo:list-item>
                	<fo:list-item>
                        <fo:list-item-label end-indent="label-end()">
                          <fo:block></fo:block>
                        </fo:list-item-label>
                        <fo:list-item-body start-indent="body-start()">
                          <fo:block>Unpaved: <xsl:value-of select="transportation/airports/ap_unpaved" /></fo:block>
                        </fo:list-item-body>
                	</fo:list-item>
                </fo:list-block>
	            <fo:block font-size="12pt" text-align="left" font-weight="bold" space-before="10px">
	            	Pipelines
	            </fo:block>
	            <fo:list-block provisional-distance-between-starts="10px"
                  start-indent="10px" font-size="10pt" space-before="3px">
                	<fo:list-item>
                        <fo:list-item-label end-indent="label-end()">
                          <fo:block></fo:block>
                        </fo:list-item-label>
                        <fo:list-item-body start-indent="body-start()">
                          <fo:block>Gas: <xsl:value-of select="transportation/pipelines/p_gas" /></fo:block>
                        </fo:list-item-body>
                	</fo:list-item>
                	<fo:list-item>
                        <fo:list-item-label end-indent="label-end()">
                          <fo:block></fo:block>
                        </fo:list-item-label>
                        <fo:list-item-body start-indent="body-start()">
                          <fo:block>Oil: <xsl:value-of select="transportation/pipelines/p_oil" /></fo:block>
                        </fo:list-item-body>
                	</fo:list-item>
                </fo:list-block>
	            <fo:block font-size="12pt" text-align="left" font-weight="bold" space-before="10px">
	            	Railways
	            </fo:block>
	            <fo:block font-size="10pt" text-align="left" space-before="10px">
	            	<xsl:value-of select="transportation/railways" />
	            </fo:block>
	            <fo:block font-size="12pt" text-align="left" font-weight="bold" space-before="10px">
	            	Roadways
	            </fo:block>
	            <fo:list-block provisional-distance-between-starts="10px"
                  start-indent="10px" font-size="10pt" space-before="3px">
                	<fo:list-item>
                        <fo:list-item-label end-indent="label-end()">
                          <fo:block></fo:block>
                        </fo:list-item-label>
                        <fo:list-item-body start-indent="body-start()">
                          <fo:block>Total: <xsl:value-of select="transportation/roadways/r_total" /></fo:block>
                        </fo:list-item-body>
                	</fo:list-item>
                	<fo:list-item>
                        <fo:list-item-label end-indent="label-end()">
                          <fo:block></fo:block>
                        </fo:list-item-label>
                        <fo:list-item-body start-indent="body-start()">
                          <fo:block>Paved: <xsl:value-of select="transportation/roadways/r_paved" /></fo:block>
                        </fo:list-item-body>
                	</fo:list-item>
                	<fo:list-item>
                        <fo:list-item-label end-indent="label-end()">
                          <fo:block></fo:block>
                        </fo:list-item-label>
                        <fo:list-item-body start-indent="body-start()">
                          <fo:block>Unpaved: <xsl:value-of select="transportation/roadways/r_unpaved" /></fo:block>
                        </fo:list-item-body>
                	</fo:list-item>
                </fo:list-block>
	            <fo:block font-size="12pt" text-align="left" font-weight="bold" space-before="10px">
	            	Waterways
	            </fo:block>
	            <fo:block font-size="10pt" text-align="left" space-before="10px">
	            	<xsl:value-of select="transportation/waterways" />
	            </fo:block>
	            <fo:block font-size="14pt" text-align="left" font-weight="bold" space-before="10px">
                <xsl:attribute name="id">
                  <xsl:value-of select="transnational_issues/@id"/>
                </xsl:attribute>
	            	Transnational issues
	            </fo:block>
	            <fo:block font-size="12pt" text-align="left" font-weight="bold" space-before="10px">
	            	Disputes
	            </fo:block>
	            <fo:block font-size="10pt" text-align="left" space-before="10px">
	            	<xsl:value-of select="transnational_issues/disputes" />
	            </fo:block>
	            <xsl:if test="count(/transnational_issues/trafficing)=1">
					<fo:block font-size="12pt" text-align="left" font-weight="bold" space-before="10px">
						Trafficing
					</fo:block>
					<fo:block font-size="10pt" text-align="left" space-before="10px">
						<xsl:value-of select="transnational_issues/trafficing/t_situation" />
					</fo:block>
					<fo:block font-size="10pt" text-align="left" space-before="3px">
						Rank 
						<xsl:value-of select="transnational_issues/trafficing/t_tier" />
						- 
						<xsl:value-of select="transnational_issues/trafficing/t_tier_note" />
					</fo:block>
				</xsl:if>
				<xsl:if test="count(/transnational_issues/drugs)=1">
					<fo:block font-size="12pt" text-align="left" font-weight="bold" space-before="10px">
						Drugs
					</fo:block>
					<fo:block font-size="10pt" text-align="left" space-before="10px">
						<xsl:value-of select="transnational_issues/drugs" />
					</fo:block>
				</xsl:if>
        </xsl:for-each>
	     </fo:flow>
    	</fo:page-sequence>
    </fo:root>
    
  </xsl:template>
</xsl:stylesheet>