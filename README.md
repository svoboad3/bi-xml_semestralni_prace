Informace k projektu

- CUBA.xml, ITALY.xml, NAMIBIA.xml a SOUTHKOREA.xml jsou xml soubory, k jednotlivým zemím
- COMBINED.xml jsou jednotlivé země skombinované do jednoho xml pomocí DTD
- template.xml jsou jednotlivé země skombinované do jednoho xml bez DTD
- template.dtd je validační DTD schéma
- template.rng je validační RelaxNG schéma

- xhtml/CUBA.html, xhtml/ITALY.html, xhtml/NAMIBIA.html a xhtml/SOUTHKOREA.html jsou html stránky pro jednotlivé země, už s manuálně přidanými obrázky
- xhtml-country-transformation.xsl je XSLT pro vygenerování html pro jednotlivé země
- INDEX-INFO.xml je xml soubor s informacemi o umístění html stránek, použit pro vygenerování index stránky
- index.html je hlavní stránka s odkazi na stránky k jednotlivím zemím
- xhtml/images/ je složka s použitými obrázky

- pdf/pdf-transformation.xsl je XSL-FO schéma pro vygenerování pdf výstupu
- pdf/pdf-default-output.pdf je pdf výstup
- pdf/pfd-output-with-pictures.pdf je pdf výstup s manuálně přidanými obrázky

Použité nástroje

- DTD validace - https://www.xmlvalidation.com/
- RelaxNG validace - https://www.liquid-technologies.com/online-relaxng-validator
- XHTML transformace - https://www.freeformatter.com/xsl-transformer.html
- PDF transformace - FOP https://xmlgraphics.apache.org/fop/
  fop -xml template.xml -xsl pdf/pdf-transformation.xsl -pdf pdf/pdf-default-output.pdf
