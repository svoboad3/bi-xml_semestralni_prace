<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
	<xsl:template match="path-info">
		<html>
	      <head>
	      	<title>select country</title>
	      </head>
	      <body>
	      	<h3>Select country</h3>
	        	<ul>
					<li><a>
						<xsl:attribute name="href">
							<xsl:value-of select="cuba/@path"/>
						</xsl:attribute>
						Cuba
					</a></li>
					<li><a>
						<xsl:attribute name="href">
							<xsl:value-of select="italy/@path"/>
						</xsl:attribute>
						Italy
					</a></li>
					<li><a>
						<xsl:attribute name="href">
							<xsl:value-of select="namibia/@path"/>
						</xsl:attribute>
						Namibia
					</a></li>
					<li><a>
						<xsl:attribute name="href">
							<xsl:value-of select="korea/@path"/>
						</xsl:attribute>
						South Korea
					</a></li>
				</ul>
	      </body>
	    </html>
	</xsl:template>
</xsl:stylesheet>