<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
<xsl:template match="/">

<html>
<head>
   <meta charset="utf-8"/>
   <title><xsl:value-of select="country/name" /></title>       
</head>
<body>
   <div style="margin: 0 10%">
   <h1><xsl:value-of select="country/name" /></h1>
   </div>
   <div style="margin: 50px 10%">
   <h3>
   <xsl:attribute name="id">
      <xsl:value-of select="country/background/@id"/>
   </xsl:attribute>Background</h3>
   <xsl:for-each select="country/background/par">
      <p><xsl:value-of select="." /></p>
   </xsl:for-each>
   <h3>
   <xsl:attribute name="id">
      <xsl:value-of select="country/geography/@id"/>
   </xsl:attribute>Geography</h3>
   <h4>Location</h4>
   <p><xsl:value-of select="country/geography/location" /></p>
   <h4>Coordinates</h4>
   <table>
   <tr>
      <td>latitude: </td>
      <td><xsl:value-of select="country/geography/coordinates/latitude" /></td>
   </tr>
   <tr>
      <td>longitude: </td>
      <td><xsl:value-of select="country/geography/coordinates/longitude" /></td>
   </tr>
   </table>
   <h4>Area</h4>
   <table>
   <tr>
      <td>Total area:</td>
      <td><xsl:value-of select="country/geography/area/a_total" /></td>
   </tr>
   <tr>
      <td>Land area: </td>
      <td><xsl:value-of select="country/geography/area/a_land" /></td>
   </tr>
   <tr>
      <td>Water area: </td>
      <td><xsl:value-of select="country/geography/area/a_water" /></td>
   </tr>
   </table>
   <h4>Climate</h4>
   <p><xsl:value-of select="country/geography/climate" /></p>
   <h4>Terrain</h4>
   <p><xsl:value-of select="country/geography/terrain" /></p>
   <h4>Elevation</h4>
   <table>
   <tr>
      <td>Mean:</td>
      <td><xsl:value-of select="country/geography/elevation/e_mean" /></td>
   </tr>
   <tr>
      <td>Lowest point: </td>
      <td><xsl:value-of select="country/geography/elevation/e_lowest" /></td>
   </tr>
   <tr>
      <td>Highest point: </td>
      <td><xsl:value-of select="country/geography/elevation/e_highest" /></td>
   </tr>
   </table>
   <h4>Resources</h4>
   <p><xsl:for-each select="country/geography/resources/resource">
      <xsl:value-of select="." />
      <xsl:if test="not (position()=last())">
         <xsl:text>, </xsl:text>
      </xsl:if>
   </xsl:for-each></p>
   <h4>Land</h4>
   <table>
   <tr>
      <td>Agricultural:</td>
      <td><xsl:value-of select="country/geography/land/l_agricultural" /></td>
   </tr>
   <tr>
      <td>Forest: </td>
      <td><xsl:value-of select="country/geography/land/l_forest" /></td>
   </tr>
   <tr>
      <td>Other: </td>
      <td><xsl:value-of select="country/geography/land/l_outher" /></td>
   </tr>
   </table>
   <h3>
   <xsl:attribute name="id">
      <xsl:value-of select="country/society/@id"/>
   </xsl:attribute>Society</h3>
   <h4>Population</h4>
   <p><xsl:value-of select="country/society/population" /></p>
   <h4>Etnicity</h4>
   <table>
      <xsl:for-each select="country/society/etnicity/etnic_group">
         <tr>
            <td><xsl:value-of select="eg_name" /><xsl:text>: </xsl:text></td>
            <td><xsl:value-of select="eg_percentage" /></td>
         </tr>
      </xsl:for-each>
   </table>
   <xsl:if test="count(/country/society/etnicity/e_note)=1">
      <p><xsl:text>note: </xsl:text><xsl:value-of select="country/society/etnicity/e_note" /></p>
   </xsl:if>
   <h4>Languages</h4>
   <p>
      <xsl:value-of select="country/society/languages/official" /><xsl:text>(official)</xsl:text>
      <xsl:for-each select="country/society/languages/language">
         <xsl:text>, </xsl:text><xsl:value-of select="." />
      </xsl:for-each>
   </p>
   <h4>Religions</h4>
   <table>
      <xsl:for-each select="country/society/religions/religion">
         <tr>
            <td><xsl:value-of select="r_name" /><xsl:text>: </xsl:text></td>
            <td><xsl:value-of select="r_percentage" /></td>
         </tr>
      </xsl:for-each>
   </table>
   <xsl:if test="count(/country/society/religions/r_note)=1">
      <p><xsl:text>note: </xsl:text><xsl:value-of select="country/society/religions/r_note" /></p>
   </xsl:if>
   <h4>Age</h4>
   <xsl:for-each select="country/society/age/a_group" >
      <p>
         <xsl:text>Age group of </xsl:text>
         <xsl:value-of select="ag_range" />
         <xsl:text>: </xsl:text>
         <xsl:value-of select="ag_percentage" />
         <xsl:text> of total population (</xsl:text>
         <xsl:value-of select="ag_male" />
         <xsl:text>  male/</xsl:text>
         <xsl:value-of select="ag_female" />
         <xsl:text>  female)</xsl:text>
      </p>
   </xsl:for-each>
   <table>
      <tr>
         <td>Total age median:</td>
         <td><xsl:value-of select="country/society/age/a_median/am_total" /></td>
      </tr>
      <tr>
         <td>Male age median: </td>
         <td><xsl:value-of select="country/society/age/a_median/am_male" /></td>
      </tr>
      <tr>
         <td>Female age median: </td>
         <td><xsl:value-of select="country/society/age/a_median/am_female" /></td>
      </tr>
      <tr>
         <td>Total life expectency:</td>
         <td><xsl:value-of select="country/society/age/a_life_expectency/al_total" /></td>
      </tr>
      <tr>
         <td>Male life expectency: </td>
         <td><xsl:value-of select="country/society/age/a_life_expectency/al_male" /></td>
      </tr>
      <tr>
         <td>Female life expectency: </td>
         <td><xsl:value-of select="country/society/age/a_life_expectency/al_female" /></td>
      </tr>
   </table>
   <h4>Urbanization</h4>
   <p>
      <xsl:text>People living in urban areas: </xsl:text>
      <xsl:value-of select="country/society/urbanization/u_population" />
      <xsl:text> of total population</xsl:text>
   </p>
   <p>
      <xsl:text>Rate of urbanization: </xsl:text>
      <xsl:value-of select="country/society/urbanization/u_rate" />
   </p>
   <p>Major urban areas:</p>
   <table>
      <xsl:for-each select="country/society/urbanization/u_major_areas/u_major_area">
         <tr>
            <td><xsl:value-of select="uma_name" /><xsl:text>: </xsl:text></td>
            <td><xsl:value-of select="uma_population" /></td>
         </tr>
      </xsl:for-each>
   </table>
   <xsl:if test="count(/country/society/literacy)=1">
      <h4>Literacy</h4>
      <table>
         <tr>
            <td>Total literacy:</td>
            <td><xsl:value-of select="country/society/literacy/l_total" /></td>
         </tr>
         <tr>
            <td>Male literacy: </td>
            <td><xsl:value-of select="country/society/age/literacy/l_male" /></td>
         </tr>
         <tr>
            <td>Female literacy: </td>
            <td><xsl:value-of select="country/society/age/literacy/l_female" /></td>
         </tr>
      </table>
   </xsl:if>
   <h3>
   <xsl:attribute name="id">
      <xsl:value-of select="country/government/@id"/>
   </xsl:attribute>Government</h3>
   <h4>Government type</h4>
   <p><xsl:value-of select="country/government/government_type" /></p>
   <h4>Capital city</h4>
   <p><xsl:value-of select="country/government/capital" /></p>
   <h4>Chief of state</h4>
   <p>
      <xsl:value-of select="country/government/chief_of_state/c_name" />
      <xsl:text> (elected </xsl:text>
      <xsl:value-of select="country/government/chief_of_state/c_elected" />
      <xsl:text>)</xsl:text>
   </p>
   <h4>Head of government</h4>
   <p>
      <xsl:value-of select="country/government/head_of_goverment/h_name" />
      <xsl:text> (elected </xsl:text>
      <xsl:value-of select="country/government/head_of_goverment/h_elected" />
      <xsl:text>)</xsl:text>
   </p>
   <h4>Legal system</h4>
   <p><xsl:value-of select="country/government/legal_system" /></p>
   <h4>State symbols</h4>
   <p>Symbols:<br />
      <xsl:for-each select="country/government/symbols/symbol">
         <xsl:value-of select="." />
         <xsl:if test="not (position()=last())">
            <xsl:text>, </xsl:text>
         </xsl:if>
      </xsl:for-each>
   </p>
   <p>Colors:<br />
      <xsl:for-each select="country/government/symbols/s_color">
         <xsl:value-of select="." />
         <xsl:if test="not (position()=last())">
            <xsl:text>, </xsl:text>
         </xsl:if>
      </xsl:for-each>
   </p>
   <p>Anthem:<br /><xsl:value-of select="country/government/symbols/anthem" /></p>
   <h3>
   <xsl:attribute name="id">
      <xsl:value-of select="country/economy/@id"/>
   </xsl:attribute>Economy</h3>
   <h4>Overview</h4>
   <xsl:for-each select="country/economy/e_overview/par">
      <p><xsl:value-of select="." /></p>
   </xsl:for-each>
   <h4>GDP</h4>
   <table>
      <tr>
         <td>GDP amount:</td>
         <td><xsl:value-of select="country/economy/GDP/GDP_amount" /></td>
      </tr>
      <tr>
         <td>GDP growth:</td>
         <td><xsl:value-of select="country/economy/GDP/GDP_growth" /></td>
      </tr>
      <tr>
         <td>GDP per capita:</td>
         <td><xsl:value-of select="country/economy/GDP/GDP_per_capita" /></td>
      </tr>
      <tr>
         <td>GDP savings:</td>
         <td><xsl:value-of select="country/economy/GDP/GDP_savings" /></td>
      </tr>
      <tr>
         <td>GDP from agriculture:</td>
         <td><xsl:value-of select="country/economy/GDP/GDP_sectors/GDPs_agriculture" /></td>
      </tr>
      <tr>
         <td>GDP from industry:</td>
         <td><xsl:value-of select="country/economy/GDP/GDP_sectors/GDPs_industry" /></td>
      </tr>
      <tr>
         <td>GDP from services:</td>
         <td><xsl:value-of select="country/economy/GDP/GDP_sectors/GDPs_services" /></td>
      </tr>
   </table>
   <h4>Agricultural products</h4>
   <p><xsl:for-each select="country/economy/agricultural_products/agricultural_product">
      <xsl:value-of select="." />
      <xsl:if test="not (position()=last())">
         <xsl:text>, </xsl:text>
      </xsl:if>
   </xsl:for-each></p>
   <h4>Industries</h4>
   <p><xsl:for-each select="country/economy/industries/industry">
      <xsl:value-of select="." />
      <xsl:if test="not (position()=last())">
         <xsl:text>, </xsl:text>
      </xsl:if>
   </xsl:for-each></p>
   <h4>Labor force</h4>
   <table>
      <tr>
         <td>Total amount:</td>
         <td><xsl:value-of select="country/economy/labor_force/lf_amount" /></td>
      </tr>
      <tr>
         <td>In agriculture:</td>
         <td><xsl:value-of select="country/economy/labor_force/lf_sectors/lfs_agriculture" /></td>
      </tr>
      <tr>
         <td>In industry:</td>
         <td><xsl:value-of select="country/economy/labor_force/lf_sectors/lfs_industry" /></td>
      </tr>
      <tr>
         <td>In services:</td>
         <td><xsl:value-of select="country/economy/labor_force/lf_sectors/lfs_services" /></td>
      </tr>
   </table>
   <h4>Exports</h4>
   <table>
      <tr>
         <td>Total amount:</td>
         <td><xsl:value-of select="country/economy/exports_and_imports/exports/e_amount" /></td>
      </tr>
      <tr>
         <td>Partners:</td>
         <td><xsl:for-each select="country/economy/exports_and_imports/exports/e_partners/e_partner">
            <xsl:value-of select="ep_name" />
            <xsl:text>(</xsl:text>
            <xsl:value-of select="ep_percentage" />
            <xsl:text>)</xsl:text>
            <xsl:if test="not (position()=last())">
               <xsl:text>, </xsl:text>
            </xsl:if>
            </xsl:for-each></td>
      </tr>
      <tr>
         <td>Commodities:</td>
         <td><xsl:for-each select="country/economy/exports_and_imports/exports/e_commodities/e_commodity">
            <xsl:value-of select="." />
            <xsl:if test="not (position()=last())">
               <xsl:text>, </xsl:text>
            </xsl:if>
            </xsl:for-each></td>
      </tr>
   </table>
   <h4>Imports</h4>
   <table>
      <tr>
         <td>Total amount:</td>
         <td><xsl:value-of select="country/economy/exports_and_imports/imports/i_amount" /></td>
      </tr>
      <tr>
         <td>Partners:</td>
         <td><xsl:for-each select="country/economy/exports_and_imports/imports/i_partners/i_partner">
            <xsl:value-of select="ip_name" />
            <xsl:text>(</xsl:text>
            <xsl:value-of select="ip_percentage" />
            <xsl:text>)</xsl:text>
            <xsl:if test="not (position()=last())">
               <xsl:text>, </xsl:text>
            </xsl:if>
            </xsl:for-each></td>
      </tr>
      <tr>
         <td>Commodities:</td>
         <td><xsl:for-each select="country/economy/exports_and_imports/imports/i_commodities/i_commodity">
            <xsl:value-of select="." />
            <xsl:if test="not (position()=last())">
               <xsl:text>, </xsl:text>
            </xsl:if>
            </xsl:for-each></td>
      </tr>
   </table>
   <h3>
   <xsl:attribute name="id">
      <xsl:value-of select="country/energy/@id"/>
   </xsl:attribute>Energy</h3>
   <h4>Electricity</h4>
   <table>
      <tr>
         <td>Access: </td>
         <td><xsl:value-of select="country/energy/electricity/e_access" /></td>
      </tr>
      <tr>
         <td>Production: </td>
         <td><xsl:value-of select="country/energy/electricity/e_production" /></td>
      </tr>
      <tr>
         <td>Consumption: </td>
         <td><xsl:value-of select="country/energy/electricity/e_consumption" /></td>
      </tr>
      <tr>
         <td>Exports: </td>
         <td><xsl:value-of select="country/energy/electricity/e_exports" /></td>
      </tr>
      <tr>
         <td>Imports: </td>
         <td><xsl:value-of select="country/energy/electricity/e_imports" /></td>
      </tr>
      <tr>
         <td>Sources:</td>
      </tr>
      <xsl:for-each select="country/energy/electricity/e_sources/e_source">
         <tr>
            <td><xsl:value-of select="es_type" /><xsl:text>: </xsl:text></td>
            <td><xsl:value-of select="es_percentage" /></td>
         </tr>
      </xsl:for-each>
   </table>
   <h4>Crude oil</h4>
   <table>
      <tr>
         <td>Production:</td>
         <td><xsl:value-of select="country/energy/crude_oil/co_production" /></td>
      </tr>
      <tr>
         <td>Exports:</td>
         <td><xsl:value-of select="country/energy/crude_oil/co_exports" /></td>
      </tr>
      <tr>
         <td>Imports:</td>
         <td><xsl:value-of select="country/energy/crude_oil/co_imports" /></td>
      </tr>
   </table>
   <h4>Refined petroleum products</h4>
   <table>
      <tr>
         <td>Production:</td>
         <td><xsl:value-of select="country/energy/refined_petroleum_products/rpp_production" /></td>
      </tr>
      <tr>
         <td>Consumption:</td>
         <td><xsl:value-of select="country/energy/refined_petroleum_products/rpp_consumption" /></td>
      </tr>
      <tr>
         <td>Exports:</td>
         <td><xsl:value-of select="country/energy/refined_petroleum_products/rpp_exports" /></td>
      </tr>
      <tr>
         <td>Imports:</td>
         <td><xsl:value-of select="country/energy/refined_petroleum_products/rpp_imports" /></td>
      </tr>
   </table>
   <h4>Natural gas</h4>
   <table>
      <tr>
         <td>Production:</td>
         <td><xsl:value-of select="country/energy/natural_gas/ng_production" /></td>
      </tr>
      <tr>
         <td>Consumption:</td>
         <td><xsl:value-of select="country/energy/natural_gas/ng_consumption" /></td>
      </tr>
      <tr>
         <td>Exports:</td>
         <td><xsl:value-of select="country/energy/natural_gas/ng_exports" /></td>
      </tr>
      <tr>
         <td>Imports:</td>
         <td><xsl:value-of select="country/energy/natural_gas/ng_imports" /></td>
      </tr>
   </table>
   <h3>
   <xsl:attribute name="id">
      <xsl:value-of select="country/communication/@id"/>
   </xsl:attribute>Communication</h3>
   <h4>Telephones</h4>
   <table>
      <tr>
         <td>Fixed lines subscribers:</td>
         <td><xsl:value-of select="country/communication/telephones/t_fixed_lines_subscribtions" /></td>
      </tr>
      <tr>
         <td>Mobile cellular subscribtions:</td>
         <td><xsl:value-of select="country/communication/telephones/t_mobile_cellular_subscribtions" /></td>
      </tr>
   </table>
   <h4>Broadcast media</h4>
   <p><xsl:value-of select="country/communication/broadcast_media" /></p>
   <h4>Internet</h4>
   <table>
      <tr>
         <td>Internet code:</td>
         <td><xsl:value-of select="country/communication/internet/i_code" /></td>
      </tr>
      <tr>
         <td>Total users:</td>
         <td><xsl:value-of select="country/communication/internet/i_total_users" /></td>
      </tr>
   </table>
   <h3>
   <xsl:attribute name="id">
      <xsl:value-of select="country/military_and_security/@id"/>
   </xsl:attribute>Military and security</h3>
   <h4>Expenditure</h4>
   <p><xsl:value-of select="country/military_and_security/expenditure" /></p>
   <h4>Military forces</h4>
   <p>
      <xsl:for-each select="country/military_and_security/military_forces/military_force" >
         <xsl:value-of select="mf_name_EN" />
         <xsl:if test="count(mf_abbreviation)=1">
            <xsl:text>(</xsl:text>
            <xsl:value-of select="mf_abbreviation" />
            <xsl:text>)</xsl:text>
         </xsl:if>
         <xsl:if test="not (position()=last())">
            <xsl:text>, </xsl:text>
         </xsl:if>
      </xsl:for-each>
   </p>
   <h4>Obligation</h4>
   <p><xsl:value-of select="country/military_and_security/obligation" /></p>
   <h3>
   <xsl:attribute name="id">
      <xsl:value-of select="country/transportation/@id"/>
   </xsl:attribute>Transportation</h3>
   <h4>Airports</h4>
   <table>
      <tr>
         <td>Total:</td>
         <td><xsl:value-of select="country/transportation/airports/ap_total" /></td>
      </tr>
      <tr>
         <td>Paved:</td>
         <td><xsl:value-of select="country/transportation/airports/ap_paved" /></td>
      </tr>
      <tr>
         <td>Unpaved:</td>
         <td><xsl:value-of select="country/transportation/airports/ap_unpaved" /></td>
      </tr>
   </table>
   <h4>Pipelines</h4>
   <table>
      <tr>
         <td>Gas:</td>
         <td><xsl:value-of select="country/transportation/pipelines/p_gas" /></td>
      </tr>
      <tr>
         <td>Oil:</td>
         <td><xsl:value-of select="country/transportation/pipelines/p_oil" /></td>
      </tr>
   </table>
   <h4>Railways</h4>
   <p><xsl:value-of select="country/transportation/railways" /></p>
   <h4>Roadways</h4>
   <table>
      <tr>
         <td>Total:</td>
         <td><xsl:value-of select="country/transportation/roadways/r_total" /></td>
      </tr>
      <tr>
         <td>Paved:</td>
         <td><xsl:value-of select="country/transportation/roadways/r_paved" /></td>
      </tr>
      <tr>
         <td>Unpaved:</td>
         <td><xsl:value-of select="country/transportation/roadways/r_unpaved" /></td>
      </tr>
   </table>
   <h4>Waterways</h4>
   <p><xsl:value-of select="country/transportation/waterways" /></p>
   <h3>
   <xsl:attribute name="id">
      <xsl:value-of select="country/transnational_issues/@id"/>
   </xsl:attribute>Transnational issues</h3>
   <h4>Disputes</h4>
   <p><xsl:value-of select="country/transnational_issues/disputes" /></p>
   <xsl:if test="count(/country/transnational_issues/trafficing)=1">
      <h4>Trafficing</h4>
      <p><xsl:value-of select="country/transnational_issues/trafficing/t_situation" /></p>
      <p>
         <xsl:text>Rank </xsl:text>
         <xsl:value-of select="country/transnational_issues/trafficing/t_tier" />
         <xsl:text>- </xsl:text>
         <xsl:value-of select="country/transnational_issues/trafficing/t_tier_note" />
      </p>
   </xsl:if>
   <xsl:if test="count(/country/transnational_issues/drugs)=1">
      <h4>Drugs</h4>
      <p><xsl:value-of select="country/transnational_issues/drugs" /></p>
   </xsl:if>
   </div>
</body>
</html>

</xsl:template>
</xsl:stylesheet>