<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
<xsl:template match="/">

<html>
<head>
	<meta charset="utf-8"/>
	<title><xsl:value-of select="country/name" /></title>			
</head>
<body>
	<div style="margin: 0 10%">
		<h1><xsl:value-of select="country/name" /></h1>
	</div>
	<div style="margin: 50px 10%">
		<xsl:apply-templates/>
	</div>
</body>
</html>

</xsl:template>

<xsl:template match="name">
</xsl:template>

<xsl:template match="background">
	<h3>
	<xsl:attribute name="id">
		<xsl:value-of select="@id"/>
	</xsl:attribute>Background</h3>
	<xsl:for-each select="par">
		<p><xsl:value-of select="." /></p>
	</xsl:for-each>
</xsl:template>

<xsl:template match="geography">	
	<h3>
	<xsl:attribute name="id">
		<xsl:value-of select="@id"/>
	</xsl:attribute>Geography</h3>
	<h4>Location</h4>
	<p><xsl:value-of select="location" /></p>
	<h4>Coordinates</h4>
	<table>
	<tr>
		<td>latitude: </td>
		<td><xsl:value-of select="coordinates/latitude" /></td>
	</tr>
	<tr>
		<td>longitude: </td>
		<td><xsl:value-of select="coordinates/longitude" /></td>
	</tr>
	</table>
	<h4>Area</h4>
	<table>
	<tr>
		<td>Total area:</td>
		<td><xsl:value-of select="area/a_total" /></td>
	</tr>
	<tr>
		<td>Land area: </td>
		<td><xsl:value-of select="area/a_land" /></td>
	</tr>
	<tr>
		<td>Water area: </td>
		<td><xsl:value-of select="area/a_water" /></td>
	</tr>
	</table>
	<h4>Climate</h4>
	<p><xsl:value-of select="climate" /></p>
	<h4>Terrain</h4>
	<p><xsl:value-of select="terrain" /></p>
	<h4>Elevation</h4>
	<table>
	<tr>
		<td>Mean:</td>
		<td><xsl:value-of select="elevation/e_mean" /></td>
	</tr>
	<tr>
		<td>Lowest point: </td>
		<td><xsl:value-of select="elevation/e_lowest" /></td>
	</tr>
	<tr>
		<td>Highest point: </td>
		<td><xsl:value-of select="elevation/e_highest" /></td>
	</tr>
	</table>
	<h4>Resources</h4>
	<p><xsl:for-each select="resources/resource">
		<xsl:value-of select="." />
		<xsl:if test="not (position()=last())">
			<xsl:text>, </xsl:text>
		</xsl:if>
	</xsl:for-each></p>
	<h4>Land</h4>
	<table>
	<tr>
		<td>Agricultural:</td>
		<td><xsl:value-of select="land/l_agricultural" /></td>
	</tr>
	<tr>
		<td>Forest: </td>
		<td><xsl:value-of select="land/l_forest" /></td>
	</tr>
	<tr>
		<td>Other: </td>
		<td><xsl:value-of select="land/l_outher" /></td>
	</tr>
	</table>
</xsl:template>

<xsl:template match="society">
	<h3>
	<xsl:attribute name="id">
		<xsl:value-of select="@id"/>
	</xsl:attribute>Society</h3>
	<h4>Population</h4>
	<p><xsl:value-of select="population" /></p>
	<h4>Etnicity</h4>
	<table>
		<xsl:for-each select="etnicity/etnic_group">
			<tr>
				<td><xsl:value-of select="eg_name" /><xsl:text>: </xsl:text></td>
				<td><xsl:value-of select="eg_percentage" /></td>
			</tr>
		</xsl:for-each>
	</table>
	<xsl:if test="count(etnicity/e_note)=1">
		<p><xsl:text>note: </xsl:text><xsl:value-of select="etnicity/e_note" /></p>
	</xsl:if>
	<h4>Languages</h4>
	<p>
		<xsl:value-of select="languages/official" /><xsl:text>(official)</xsl:text>
		<xsl:for-each select="languages/language">
			<xsl:text>, </xsl:text><xsl:value-of select="." />
		</xsl:for-each>
	</p>
	<h4>Religions</h4>
	<table>
		<xsl:for-each select="religions/religion">
			<tr>
				<td><xsl:value-of select="r_name" /><xsl:text>: </xsl:text></td>
				<td><xsl:value-of select="r_percentage" /></td>
			</tr>
		</xsl:for-each>
	</table>
	<xsl:if test="count(religions/r_note)=1">
		<p><xsl:text>note: </xsl:text><xsl:value-of select="religions/r_note" /></p>
	</xsl:if>
	<h4>Age</h4>
	<xsl:for-each select="age/a_group" >
		<p>
			<xsl:text>Age group of </xsl:text>
			<xsl:value-of select="ag_range" />
			<xsl:text>: </xsl:text>
			<xsl:value-of select="ag_percentage" />
			<xsl:text> of total population (</xsl:text>
			<xsl:value-of select="ag_male" />
			<xsl:text>  male/</xsl:text>
			<xsl:value-of select="ag_female" />
			<xsl:text>  female)</xsl:text>
		</p>
	</xsl:for-each>
	<table>
		<tr>
			<td>Total age median:</td>
			<td><xsl:value-of select="age/a_median/am_total" /></td>
		</tr>
		<tr>
			<td>Male age median: </td>
			<td><xsl:value-of select="age/a_median/am_male" /></td>
		</tr>
		<tr>
			<td>Female age median: </td>
			<td><xsl:value-of select="age/a_median/am_female" /></td>
		</tr>
		<tr>
			<td>Total life expectency:</td>
			<td><xsl:value-of select="age/a_life_expectency/al_total" /></td>
		</tr>
		<tr>
			<td>Male life expectency: </td>
			<td><xsl:value-of select="age/a_life_expectency/al_male" /></td>
		</tr>
		<tr>
			<td>Female life expectency: </td>
			<td><xsl:value-of select="age/a_life_expectency/al_female" /></td>
		</tr>
	</table>
	<h4>Urbanization</h4>
	<p>
		<xsl:text>People living in urban areas: </xsl:text>
		<xsl:value-of select="urbanization/u_population" />
		<xsl:text> of total population</xsl:text>
	</p>
	<p>
		<xsl:text>Rate of urbanization: </xsl:text>
		<xsl:value-of select="urbanization/u_rate" />
	</p>
	<p>Major urban areas:</p>
	<table>
		<xsl:for-each select="urbanization/u_major_areas/u_major_area">
			<tr>
				<td><xsl:value-of select="uma_name" /><xsl:text>: </xsl:text></td>
				<td><xsl:value-of select="uma_population" /></td>
			</tr>
		</xsl:for-each>
	</table>
	<xsl:if test="count(literacy)=1">
		<h4>Literacy</h4>
		<table>
			<tr>
				<td>Total literacy:</td>
				<td><xsl:value-of select="literacy/l_total" /></td>
			</tr>
			<tr>
				<td>Male literacy: </td>
				<td><xsl:value-of select="age/literacy/l_male" /></td>
			</tr>
			<tr>
				<td>Female literacy: </td>
				<td><xsl:value-of select="age/literacy/l_female" /></td>
			</tr>
		</table>
	</xsl:if>
</xsl:template>


<xsl:template match="government">
	<h3>
	<xsl:attribute name="id">
		<xsl:value-of select="@id"/>
	</xsl:attribute>Government</h3>
	<h4>Government type</h4>
	<p><xsl:value-of select="government_type" /></p>
	<h4>Capital city</h4>
	<p><xsl:value-of select="capital" /></p>
	<h4>Chief of state</h4>
	<p>
		<xsl:value-of select="chief_of_state/c_name" />
		<xsl:text> (elected </xsl:text>
		<xsl:value-of select="chief_of_state/c_elected" />
		<xsl:text>)</xsl:text>
	</p>
	<h4>Head of government</h4>
	<p>
		<xsl:value-of select="head_of_goverment/h_name" />
		<xsl:text> (elected </xsl:text>
		<xsl:value-of select="head_of_goverment/h_elected" />
		<xsl:text>)</xsl:text>
	</p>
	<h4>Legal system</h4>
	<p><xsl:value-of select="legal_system" /></p>
	<h4>State symbols</h4>
	<p>Symbols:<br />
		<xsl:for-each select="symbols/symbol">
			<xsl:value-of select="." />
			<xsl:if test="not (position()=last())">
				<xsl:text>, </xsl:text>
			</xsl:if>
		</xsl:for-each>
	</p>
	<p>Colors:<br />
		<xsl:for-each select="symbols/s_color">
			<xsl:value-of select="." />
			<xsl:if test="not (position()=last())">
				<xsl:text>, </xsl:text>
			</xsl:if>
		</xsl:for-each>
	</p>
	<p>Anthem:<br /><xsl:value-of select="symbols/anthem" /></p>
</xsl:template>


<xsl:template match="economy">
	<h3>
	<xsl:attribute name="id">
		<xsl:value-of select="@id"/>
	</xsl:attribute>Economy</h3>
	<h4>Overview</h4>
	<xsl:for-each select="e_overview/par">
		<p><xsl:value-of select="." /></p>
	</xsl:for-each>
	<h4>GDP</h4>
	<table>
		<tr>
			<td>GDP amount:</td>
			<td><xsl:value-of select="GDP/GDP_amount" /></td>
		</tr>
		<tr>
			<td>GDP growth:</td>
			<td><xsl:value-of select="GDP/GDP_growth" /></td>
		</tr>
		<tr>
			<td>GDP per capita:</td>
			<td><xsl:value-of select="GDP/GDP_per_capita" /></td>
		</tr>
		<tr>
			<td>GDP savings:</td>
			<td><xsl:value-of select="GDP/GDP_savings" /></td>
		</tr>
		<tr>
			<td>GDP from agriculture:</td>
			<td><xsl:value-of select="GDP/GDP_sectors/GDPs_agriculture" /></td>
		</tr>
		<tr>
			<td>GDP from industry:</td>
			<td><xsl:value-of select="GDP/GDP_sectors/GDPs_industry" /></td>
		</tr>
		<tr>
			<td>GDP from services:</td>
			<td><xsl:value-of select="GDP/GDP_sectors/GDPs_services" /></td>
		</tr>
	</table>
	<h4>Agricultural products</h4>
	<p><xsl:for-each select="agricultural_products/agricultural_product">
		<xsl:value-of select="." />
		<xsl:if test="not (position()=last())">
			<xsl:text>, </xsl:text>
		</xsl:if>
	</xsl:for-each></p>
	<h4>Industries</h4>
	<p><xsl:for-each select="industries/industry">
		<xsl:value-of select="." />
		<xsl:if test="not (position()=last())">
			<xsl:text>, </xsl:text>
		</xsl:if>
	</xsl:for-each></p>
	<h4>Labor force</h4>
	<table>
		<tr>
			<td>Total amount:</td>
			<td><xsl:value-of select="labor_force/lf_amount" /></td>
		</tr>
		<tr>
			<td>In agriculture:</td>
			<td><xsl:value-of select="labor_force/lf_sectors/lfs_agriculture" /></td>
		</tr>
		<tr>
			<td>In industry:</td>
			<td><xsl:value-of select="labor_force/lf_sectors/lfs_industry" /></td>
		</tr>
		<tr>
			<td>In services:</td>
			<td><xsl:value-of select="labor_force/lf_sectors/lfs_services" /></td>
		</tr>
	</table>
	<h4>Exports</h4>
	<table>
		<tr>
			<td>Total amount:</td>
			<td><xsl:value-of select="exports_and_imports/exports/e_amount" /></td>
		</tr>
		<tr>
			<td>Partners:</td>
			<td><xsl:for-each select="exports_and_imports/exports/e_partners/e_partner">
				<xsl:value-of select="ep_name" />
				<xsl:text>(</xsl:text>
				<xsl:value-of select="ep_percentage" />
				<xsl:text>)</xsl:text>
				<xsl:if test="not (position()=last())">
					<xsl:text>, </xsl:text>
				</xsl:if>
				</xsl:for-each></td>
		</tr>
		<tr>
			<td>Commodities:</td>
			<td><xsl:for-each select="exports_and_imports/exports/e_commodities/e_commodity">
				<xsl:value-of select="." />
				<xsl:if test="not (position()=last())">
					<xsl:text>, </xsl:text>
				</xsl:if>
				</xsl:for-each></td>
		</tr>
	</table>
	<h4>Imports</h4>
	<table>
		<tr>
			<td>Total amount:</td>
			<td><xsl:value-of select="exports_and_imports/imports/i_amount" /></td>
		</tr>
		<tr>
			<td>Partners:</td>
			<td><xsl:for-each select="exports_and_imports/imports/i_partners/i_partner">
				<xsl:value-of select="ip_name" />
				<xsl:text>(</xsl:text>
				<xsl:value-of select="ip_percentage" />
				<xsl:text>)</xsl:text>
				<xsl:if test="not (position()=last())">
					<xsl:text>, </xsl:text>
				</xsl:if>
				</xsl:for-each></td>
		</tr>
		<tr>
			<td>Commodities:</td>
			<td><xsl:for-each select="exports_and_imports/imports/i_commodities/i_commodity">
				<xsl:value-of select="." />
				<xsl:if test="not (position()=last())">
					<xsl:text>, </xsl:text>
				</xsl:if>
				</xsl:for-each></td>
		</tr>
	</table>
</xsl:template>

<xsl:template match="energy">
	<h3>
	<xsl:attribute name="id">
		<xsl:value-of select="@id"/>
	</xsl:attribute>Energy</h3>
	<h4>Electricity</h4>
	<table>
		<tr>
			<td>Access: </td>
			<td><xsl:value-of select="electricity/e_access" /></td>
		</tr>
		<tr>
			<td>Production: </td>
			<td><xsl:value-of select="electricity/e_production" /></td>
		</tr>
		<tr>
			<td>Consumption: </td>
			<td><xsl:value-of select="electricity/e_consumption" /></td>
		</tr>
		<tr>
			<td>Exports: </td>
			<td><xsl:value-of select="electricity/e_exports" /></td>
		</tr>
		<tr>
			<td>Imports: </td>
			<td><xsl:value-of select="electricity/e_imports" /></td>
		</tr>
		<tr>
			<td>Sources:</td>
		</tr>
		<xsl:for-each select="electricity/e_sources/e_source">
			<tr>
				<td><xsl:value-of select="es_type" /><xsl:text>: </xsl:text></td>
				<td><xsl:value-of select="es_percentage" /></td>
			</tr>
		</xsl:for-each>
	</table>
	<h4>Crude oil</h4>
	<table>
		<tr>
			<td>Production:</td>
			<td><xsl:value-of select="crude_oil/co_production" /></td>
		</tr>
		<tr>
			<td>Exports:</td>
			<td><xsl:value-of select="crude_oil/co_exports" /></td>
		</tr>
		<tr>
			<td>Imports:</td>
			<td><xsl:value-of select="crude_oil/co_imports" /></td>
		</tr>
	</table>
	<h4>Refined petroleum products</h4>
	<table>
		<tr>
			<td>Production:</td>
			<td><xsl:value-of select="refined_petroleum_products/rpp_production" /></td>
		</tr>
		<tr>
			<td>Consumption:</td>
			<td><xsl:value-of select="refined_petroleum_products/rpp_consumption" /></td>
		</tr>
		<tr>
			<td>Exports:</td>
			<td><xsl:value-of select="refined_petroleum_products/rpp_exports" /></td>
		</tr>
		<tr>
			<td>Imports:</td>
			<td><xsl:value-of select="refined_petroleum_products/rpp_imports" /></td>
		</tr>
	</table>
	<h4>Natural gas</h4>
	<table>
		<tr>
			<td>Production:</td>
			<td><xsl:value-of select="natural_gas/ng_production" /></td>
		</tr>
		<tr>
			<td>Consumption:</td>
			<td><xsl:value-of select="natural_gas/ng_consumption" /></td>
		</tr>
		<tr>
			<td>Exports:</td>
			<td><xsl:value-of select="natural_gas/ng_exports" /></td>
		</tr>
		<tr>
			<td>Imports:</td>
			<td><xsl:value-of select="natural_gas/ng_imports" /></td>
		</tr>
	</table>
</xsl:template>

<xsl:template match="communication">
	<h3>
	<xsl:attribute name="id">
		<xsl:value-of select="@id"/>
	</xsl:attribute>Communication</h3>
	<h4>Telephones</h4>
	<table>
		<tr>
			<td>Fixed lines subscribers:</td>
			<td><xsl:value-of select="telephones/t_fixed_lines_subscribtions" /></td>
		</tr>
		<tr>
			<td>Mobile cellular subscribtions:</td>
			<td><xsl:value-of select="telephones/t_mobile_cellular_subscribtions" /></td>
		</tr>
	</table>
	<h4>Broadcast media</h4>
	<p><xsl:value-of select="broadcast_media" /></p>
	<h4>Internet</h4>
	<table>
		<tr>
			<td>Internet code:</td>
			<td><xsl:value-of select="internet/i_code" /></td>
		</tr>
		<tr>
			<td>Total users:</td>
			<td><xsl:value-of select="internet/i_total_users" /></td>
		</tr>
	</table>
</xsl:template>

<xsl:template match="military_and_security">
	<h3>
	<xsl:attribute name="id">
		<xsl:value-of select="@id"/>
	</xsl:attribute>Military and security</h3>
	<h4>Expenditure</h4>
	<p><xsl:value-of select="expenditure" /></p>
	<h4>Military forces</h4>
	<p>
		<xsl:for-each select="military_forces/military_force" >
			<xsl:value-of select="mf_name_EN" />
			<xsl:if test="count(mf_abbreviation)=1">
				<xsl:text>(</xsl:text>
				<xsl:value-of select="mf_abbreviation" />
				<xsl:text>)</xsl:text>
			</xsl:if>
			<xsl:if test="not (position()=last())">
				<xsl:text>, </xsl:text>
			</xsl:if>
		</xsl:for-each>
	</p>
	<h4>Obligation</h4>
	<p><xsl:value-of select="obligation" /></p>
</xsl:template>

<xsl:template match="transportation">
	<h3>
	<xsl:attribute name="id">
		<xsl:value-of select="@id"/>
	</xsl:attribute>Transportation</h3>
	<h4>Airports</h4>
	<table>
		<tr>
			<td>Total:</td>
			<td><xsl:value-of select="airports/ap_total" /></td>
		</tr>
		<tr>
			<td>Paved:</td>
			<td><xsl:value-of select="airports/ap_paved" /></td>
		</tr>
		<tr>
			<td>Unpaved:</td>
			<td><xsl:value-of select="airports/ap_unpaved" /></td>
		</tr>
	</table>
	<h4>Pipelines</h4>
	<table>
		<tr>
			<td>Gas:</td>
			<td><xsl:value-of select="pipelines/p_gas" /></td>
		</tr>
		<tr>
			<td>Oil:</td>
			<td><xsl:value-of select="pipelines/p_oil" /></td>
		</tr>
	</table>
	<h4>Railways</h4>
	<p><xsl:value-of select="railways" /></p>
	<h4>Roadways</h4>
	<table>
		<tr>
			<td>Total:</td>
			<td><xsl:value-of select="roadways/r_total" /></td>
		</tr>
		<tr>
			<td>Paved:</td>
			<td><xsl:value-of select="roadways/r_paved" /></td>
		</tr>
		<tr>
			<td>Unpaved:</td>
			<td><xsl:value-of select="roadways/r_unpaved" /></td>
		</tr>
	</table>
	<h4>Waterways</h4>
	<p><xsl:value-of select="waterways" /></p>
</xsl:template>

<xsl:template match="transnational_issues">
	<h3>
	<xsl:attribute name="id">
		<xsl:value-of select="@id"/>
	</xsl:attribute>Transnational issues</h3>
	<h4>Disputes</h4>
	<p><xsl:value-of select="disputes" /></p>
	<xsl:if test="count(trafficing)=1">
		<h4>Trafficing</h4>
		<p><xsl:value-of select="trafficing/t_situation" /></p>
		<p>
			<xsl:text>Rank </xsl:text>
			<xsl:value-of select="trafficing/t_tier" />
			<xsl:text>- </xsl:text>
			<xsl:value-of select="trafficing/t_tier_note" />
		</p>
	</xsl:if>
	<xsl:if test="count(drugs)=1">
		<h4>Drugs</h4>
		<p><xsl:value-of select="drugs" /></p>
	</xsl:if>
</xsl:template>

</xsl:stylesheet>